﻿// Паттерн Снимок
//
// Назначение: Позволяет делать снимки состояния объектов, не раскрывая
// подробностей их реализации. Затем снимки можно использовать, чтобы
// восстановить прошлое состояние объектов.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Memento
{

    //Класс для хранения состояния
  class Originator
  {

    private string _state;

    public Originator(string state)
    {
      this._state = state;
      Console.WriteLine("Initial state is: " + state);
    }

 //Логика выстрелов
    public void DoShot()
    {
      Console.WriteLine("Shooting: ");
      this._state = this.GenerateShotQueue(30);
      Console.WriteLine($"State changed to: {_state}");
    }

    private string GenerateShotQueue(int length = 10)
    {
      string allowedSymbols = "******************************";
      string result = string.Empty;

      while (length > 0)
      {
        result += allowedSymbols[new Random().Next(0, allowedSymbols.Length)];

        Thread.Sleep(12);

        length--;
      }

      return result;
    }


    public IMemento Save()
    {
      return new ConcreteMemento(this._state);
    }


    public void Restore(IMemento memento)
    {
      if (!(memento is ConcreteMemento))
      {
        throw new Exception("Unknown memento class " + memento.ToString());
      }

      this._state = memento.GetState();
      Console.Write($"State has changed to: {_state}");
    }
  }


  public interface IMemento
  {
    string GetName();

    string GetState();

    DateTime GetDate();
  }


  class ConcreteMemento : IMemento
  {
    private string _state;

    private DateTime _date;

    public ConcreteMemento(string state)
    {
      this._state = state;
      this._date = DateTime.Now;
    }

    public string GetState()
    {
      return this._state;
    }


    public string GetName()
    {
      return $"{this._date} / ({this._state.Substring(0, 9)})...";
    }

    public DateTime GetDate()
    {
      return this._date;
    }
  }

    //Опекун не имеет доступа во внутрь класса создателя, только к листу состоянии
  class Caretaker
  {
    private List<IMemento> _mementos = new List<IMemento>();

    private Originator _originator = null;

    public Caretaker(Originator originator)
    {
      this._originator = originator;
    }

    public void Backup()
    {
      Console.WriteLine("\nCaretaker: Saving state...");
      this._mementos.Add(this._originator.Save());
    }

    public void Undo()
    {
      if (this._mementos.Count == 0)
      {
        return;
      }

      var memento = this._mementos.Last();
      this._mementos.Remove(memento);

      Console.WriteLine("Caretaker: Restoring state to: " + memento.GetName());

      try
      {
        this._originator.Restore(memento);
      }
      catch (Exception)
      {
        this.Undo();
      }
    }

    public void ShowHistory()
    {
      Console.WriteLine("Caretaker: Here's the list of mementos:");

      foreach (var memento in this._mementos)
      {
        Console.WriteLine(memento.GetName());
      }
    }
  }

  class Program
  {
    static void Main(string[] args)
    {

      Originator originator = new Originator("***************************");
      Caretaker caretaker = new Caretaker(originator);

      caretaker.Backup();
      originator.DoShot();

      caretaker.Backup();
      originator.DoShot();

      caretaker.Backup();
      originator.DoShot();

      Console.WriteLine();
      caretaker.ShowHistory();

      Console.WriteLine("\nClient: Rollback!\n");
      caretaker.Undo();

      Console.WriteLine();
      Console.ReadLine();
    }
  }
}
