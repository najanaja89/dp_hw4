﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chain_of_responsibility
{
    public interface IHandler
    {
        IHandler SetNext(IHandler handler);

        object Handle(object request);
    }

    abstract class AbstractHandler : IHandler
    {
        private IHandler _nextHandler;

        public IHandler SetNext(IHandler handler)
        {
            this._nextHandler = handler;
            return handler;
        }

        public virtual object Handle(object request)
        {
            if (this._nextHandler != null)
            {
                return this._nextHandler.Handle(request);
            }
            else
            {
                return null;
            }
        }
    }

    class Message200 : AbstractHandler
    {
        public override object Handle(object request)
        {
            if ((request as string) == "200")
            {
                return $"{request.ToString()} OK Page Loaded.\n";
            }
            else
            {
                return base.Handle(request);
            }
        }
    }

    class Message400 : AbstractHandler
    {
        public override object Handle(object request)
        {
            if ((request as string) == "400")
            {
                return $"{request.ToString()} Bad Request, Server can't serve request.\n";
            }
            else
            {
                return base.Handle(request);
            }
        }
    }

    class Message401 : AbstractHandler
    {
        public override object Handle(object request)
        {
            if ((request as string) == "401")
            {
                return $"{request.ToString()} Unauthorized, permission denied.\n";
            }
            else
            {
                return base.Handle(request);
            }
        }
    }


    class Message501 : AbstractHandler
    {
        public override object Handle(object request)
        {
            
            if ((request as string) == "501")
            {
                return $"{request.ToString()} Not Implemented, server don't support request.\n";
            }
            else
            {
                return base.Handle(request);
            }
        }
    }

    class Client
    {
        public static void ClientCode(AbstractHandler handler)
        {
            foreach (var errorCodes in new List<string> { "200", "400", "401", "501" })
            {
                Console.WriteLine($"Client: Request to server {errorCodes}");

                var result = handler.Handle(errorCodes);

                if (result != null)
                {
                    Console.Write($"   {result}");
                }
                else
                {
                    Console.WriteLine($"{errorCodes} unknown request.");
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var message200 = new Message200();
            var message400 = new Message400();
            var message401 = new Message401();
            var message501 = new Message501();


            message400.SetNext(message501).SetNext(message200);

            Console.WriteLine("Client send request");
            Client.ClientCode(message400);
            Console.ReadLine();
        }
    }
}
